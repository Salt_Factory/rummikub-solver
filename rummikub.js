class Game {
  constructor() {
    // Keep an array of tiles.
    this.boardTiles = [];
    this.playerTiles = [];
    this.topTiles = [];

    // Keep an array of buttons as well.
    this.buttons = [];

    // If a tile is being dragged.
    this.dragTile = null;

    // Init a bunch of things.
    this.initTopTiles();
    this.initButtons();

    this.nbSets = 0;
  }

  initTopTiles() {
    const color = "blue";
    for (var number = 1; number < 14; number++) {
      const newTile = new Tile(number, color, number * 50, 0);
      this.topTiles.push(newTile);
    }
    const blue_joker = new Tile('jkr', 'blue', 14 * 50, 0);
    const red_joker = new Tile('jkr', 'red', 15 * 50, 0);
    this.topTiles.push(blue_joker);
    this.topTiles.push(red_joker);
  }

  initButtons() {
    this.buttons.push(new Button('blue',  16 * 50, 12.5));
    this.buttons.push(new Button('red',  17 * 50, 12.5));
    this.buttons.push(new Button('orange',  18 * 50, 12.5));
    this.buttons.push(new Button('black',  19 * 50, 12.5));
    this.buttons.push(new textButton('Solve', 0, 5));
    //this.buttons.push(new textButton('Solve', 0, 30));
  }

  changeTopColor(color) {
    // Change the color of all tiles, except for the 2 jokers.
    for (var i = 0; i < this.topTiles.length-2; i++) {
      this.topTiles[i].color = color;
    }
  }

  /*
   * Assign indices to the tiles in the different sets, to denote what set they belong to.
   */
  assignIndices() {
    // Loop over every tile, and find the ones that do not have a leftTile.
    for (var i = 0; i < this.boardTiles.length; i++) {
      // If no leftTile, then this is the left-most tile. Start a new set here.
      if (!this.boardTiles[i].leftTile) {
        // Recursively assign every tile in the set the same index.
        this.boardTiles[i].assignIndex(this.nbSets);
        this.nbSets = this.nbSets + 1;
      }
    }
  }

  /*
   * Update the game with the data contained in the json that was received from the server.
   */
  updateWithJson(data) {
    console.log(this.boardTiles);
    console.log(data);

    // Clear the board tiles, player tiles.
    this.boardTiles = [];
    this.playerTiles = [];

    // Go over every run and recreate its tiles.
    let cur_y = 85;
    let cur_x = 30;
    for (var run_id in data['run']) {
      const run = data['run'][run_id];

      run.sort((firstEl, secondEl) => {
        const split1 = firstEl.split('_');
        const split2 = secondEl.split('_');

        return (parseInt(split1[1]) > parseInt(split2[1]));
      });

      for (var tile_id = 0; tile_id < run.length; tile_id++) {
        const tile_name = run[tile_id];
        const split = tile_name.split('_');
        const color = split[0];
        const number = split[1];

        // Create a new tile.
        const tile = new Tile(number, color, cur_x, cur_y);
        cur_x += tile.width;

        // Check if this tile touches any tiles already on the board. If so, do not add it.
        let noTouch = true
        for (var otherTileId = 0; otherTileId < this.boardTiles.length; otherTileId++) {
          const otherTile = this.boardTiles[otherTileId];
          if (otherTile.touches(tile) || tile.touches(otherTile)) {
            tile_id--;
            noTouch = false;
            break;
          }
        }
        
        if (noTouch) {
          this.boardTiles.push(tile);
          console.log(tile);
        }
      }
      // Adjust the coordinates of for the next run.
      cur_x = 30;
      cur_y += 55 * 1.2;

      if (cur_y > 400) {
        cur_y = 85
        cur_x = 100
      }
    }
    // Go over every group and recreate its tiles.
    for (var group_id in data['group']) {
      const group = data['group'][group_id];
      for (var tile_id = 0; tile_id < group.length; tile_id++) {
        const tile_name = group[tile_id];
        const split = tile_name.split('_');
        const color = split[0];
        const number = split[1];

        // Create a new tile.
        const tile = new Tile(number, color, cur_x, cur_y);
        cur_x += tile.width;

        // Check if this tile touches any tiles already on the board. If so, do not add it.
        let noTouch = true
        for (var otherTileId = 0; otherTileId < this.boardTiles.length; otherTileId++) {
          const otherTile = this.boardTiles[otherTileId];
          if (otherTile.touches(tile) || tile.touches(otherTile)) {
            tile_id--;
            noTouch = false;
            break;
          }
        }

        if (noTouch) {
          this.boardTiles.push(tile);
          console.log(tile);
        }
      }
      // Adjust the coordinates of for the next group.
      cur_x = 40;
      cur_y += 55 * 1.2;
      if (cur_y > 400) {
        cur_y = 85
        cur_x = 100
      }
    }
    // Go over every tile that is left in hand, and add it again.
    cur_x = 30;
    cur_y = 530;
    const in_hand = data['in_hand'];
    for (var tile_id = 0; tile_id < in_hand.length; tile_id++) {
      const tile_name = in_hand[tile_id];
      if (tile_name === "") {
        continue
      }
      const split = tile_name.split('_');
      const color = split[0];
      const number = split[1];

      console.log(split, color, number);
      // Create a new tile.
      const tile = new Tile(number, color, cur_x, cur_y);
      cur_x += tile.width * 1.2;
      this.playerTiles.push(tile);
    }

  }

  /*
   * Function called by the canvas whenever a mouseDown event is registered.
   */
  checkMouseDown(mouseX, mouseY) {
    // Check if the event happened in the top tray.
    // Otherwise, it happened in the playing field.
    if (mouseY < 60) {
      if (45 < mouseX && mouseX < 16 * 50) {
        // If a tile is clicked, we want to drag it.
        for (var i = 0; i < this.topTiles.length; i++) {
          const tile = this.topTiles[i];
          if (tile.isClicked(mouseX, mouseY)) {
            this.dragTile = new Tile(tile.number, tile.color, mouseX, mouseY);
            break;
          }
        }
      }
    } else if (mouseY > 500) {
      // If a playertile is clicked, we also want to drag it & delete the original.
      for (var i = 0; i < this.playerTiles.length; i++) {
        const tile = this.playerTiles[i];
        if (tile.isClicked(mouseX, mouseY)) {
          this.playerTiles.splice(i, 1);
          this.dragTile = tile;
          break;
        }
      }
    } else {
      // If a tile is clicked in the board, we want to drag it.
      // However, we delete the original to avoid copying it.
      for (var i = 0; i < this.boardTiles.length; i++) {
        const tile = this.boardTiles[i];
        if (tile.isClicked(mouseX, mouseY)) {
          // First, unlink it from its neighbours.
          if (tile.leftTile) {
            tile.leftTile.rightTile = null;
            tile.leftTile = null;
          }
          if (tile.rightTile) {
            tile.rightTile.leftTile = null;
            tile.rightTile = null;
          }
          // Delete the tile (it gets readded as soon as we drop it again).
          this.boardTiles.splice(i, 1);
          this.dragTile = tile;
          break;
        }
      }

    }


  }
  /*
   * Function called by the canvas whenever a mouseUp event is registered.
   */
  checkMouseUp(mouseX, mouseY) {
    // Check if the event happened in the top tray.
    // Otherwise, it happened in the playing field.
    if (mouseY < 60) {
      // Check if event happened in the tiles section or the buttons section.
      if (45 < mouseX && mouseX < 16 * 50) {
        // If mouseup on top row, we delete the tile that is being dragged.
        this.dragTile = null;
      } else if (mouseX < 45) {
        for (var i = 0; i < this.buttons.length; i++) {
          const button = this.buttons[i];
          if (button.isClicked(mouseX, mouseY)) {
            const method = button.text;
            if (method === 'Solve') {
              checkTiles();
            }
          }
        }
      } else {
        // Check which button was clicked.
        for (var i = 0; i < this.buttons.length; i++) {
          const button = this.buttons[i];
          if (button.isClicked(mouseX, mouseY)) {
            this.changeTopColor(button.color);
            break;
          }
        }
      }
    } else if (mouseY > 500) {
      // If the tile is dropped in the bottom, we don't need to do anything fancy with snapping.
      if (this.dragTile) {
        this.playerTiles.push(this.dragTile);
      }
      this.dragTile = null;
    } else {
      // If a tile is being dragged, we should drop it.
      if (this.dragTile) {
        // First, check if it should snap to another tile and set its coordinates accordingly.
        for (var i = 0; i < this.boardTiles.length; i++) {
          const tile = this.boardTiles[i];
          const new_coord = tile.checkSnap(mouseX, mouseY);
          if (new_coord) {
            this.dragTile.x = new_coord[1];
            this.dragTile.y = new_coord[2];

            if (new_coord[0] == 'left') {
              this.dragTile.rightTile = tile;
              tile.leftTile = this.dragTile;
            } else {
              this.dragTile.leftTile = tile;
              tile.rightTile = this.dragTile;
            }
            break;
          }
        }
        this.boardTiles.push(this.dragTile);
      }
      this.dragTile = null;
    }
  }

  /*
   * Function called by canvas whenever a mouseMove event is registered.
   */
  checkMouseMove(mouseX, mouseY) {
    // If a tile is being dragged, we want to update its position to the mouse. 
    if (this.dragTile) {
      this.dragTile.x = mouseX;
      this.dragTile.y = mouseY;
    }

  }
}

class Button {
  constructor(color, x, y) {
    this.color = color;
    this.x = x;
    this.y = y;
    this.width = 30;
    this.height = 30;
  }

  isClicked(mouseX, mouseY) {
    if ((this.x <= mouseX && mouseX <= this.x + this.width) && (this.y <= mouseY && mouseY <= this.y + this.height)) {
      return true;
    }
    return false;
  }
}

class textButton extends Button {
  constructor(text, x, y) {
    super(null, x, y);
    this.text = text
    this.width = 40;
    this.height = 20;
  }
}

class Tile extends Button {
  constructor(number, color, x=0, y=0) {
    super(color, x, y);
    this.number = number;
    this.width = 40;
    this.height = 55;
    this.setId = null;
    this.leftTile = null;
    this.rightTile = null;
  }

  toJson() {
    return {'number': this.number, 'color': this.color, 'set': this.setId}
  }

  /*
   * Recursively assign a set index to this tile, and every tile to the right of it.
   */
  assignIndex(setId) {
    this.setId = setId;
    if (this.rightTile) {
      this.rightTile.assignIndex(setId);
    }
    return
  }

  /*
   * Check if a tile that is being dragged should snap to this one, either from the left or the right, and what its coordinates should be.
   */
  checkSnap(mouseX, mouseY) {
    // The 'snap zone' extends outside of the tile, both to the side and to the top.
    // First, we check a right snap.
    if (this.x + this.width <= mouseX && mouseX <= (this.x + 1.5 * this.width) && this.y - 10 <= mouseY && mouseY <= this.y + this.height + 10) {
      return ['right', this.x+this.width, this.y];
    }
    if (this.x - this.width <= mouseX && mouseX <= this.x && this.y - 10 <= mouseY && mouseY <= this.y + this.height + 10) {
      return ['left', this.x-this.width, this.y];
    }
  }

  /*
   * Check if two tiles overlap.
   */
  touches(otherTile) {
    if ((this.x < otherTile.x && otherTile.x < this.x + this.width) && (this.y <= otherTile.y && otherTile.y <= this.y)) {
      return true;
    }
    return false;
  }

}

/*
 * Convert the current board layout to Idp code
 */
function convertToJson() {
  game.assignIndices();
  boardTiles = [];
  playerTiles = [];
  for (var i = 0; i < game.boardTiles.length; i++) {
    const tile = game.boardTiles[i];
    const name = tile.color + "_" + tile.number + "_" + "tile_board_" + i;
    boardTiles.push({'name': name, 'tile': tile.toJson()})
  }
  for (var i = 0; i < game.playerTiles.length; i++) {
    const tile = game.playerTiles[i];
    const name = tile.color + "_" + tile.number + "_" + "tile_player_" + i;
    playerTiles.push({'name': name, 'tile': tile.toJson()})
  }

  const json = {
    'board': boardTiles,
    'player': playerTiles,
    'inference': null
  }
  return json;
}

/*
 * Send post request to server.
 */
function doPost(json) {
  const url = "localhost";
  //var params = "lorem=ipsum&name=alpha";
  var xhr = new XMLHttpRequest();
  xhr.open("POST", '', true);
  xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8');

  xhr.send(JSON.stringify(json));
  xhr.onloadend = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      console.log(xhr.responseText);
      const json = JSON.parse(xhr.responseText);
      game.updateWithJson(json);
      //console.log(json);
    }
    console.log('Done!');
  };
  console.log('Sent POST');

  //Send the proper header information along with the request
  //xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  //
  //xhr.send(params);
  //
}

function checkTiles() {
  console.log('checktiles');
  // The rummikub json contains three things:
  // 1. All board tiles.
  // 2. All player tiles.
  // 3. Inference method (check or solve)
  idp = convertToJson()
  idp['inference'] = 'check';
  doPost(idp);
}

function drawField() {
  ctx.beginPath();
  ctx.lineWidth = '2';
  ctx.strokeStyle = 'black';
  ctx.moveTo(0, 60);
  ctx.lineTo(1000, 60);
  ctx.stroke();
  ctx.moveTo(0, 500);
  ctx.lineTo(1000, 500);
  ctx.stroke();

  // Draw "board" and "player" text labels.
  ctx.font = "20px Arial";
  ctx.fillStyle = 'black';
  ctx.fillText('Board', 10, 80);
  ctx.fillText('Player', 10, 520);
}
function drawTile(tile) {
  // Draw the tile rectangle -- in black.
  ctx.beginPath();
  ctx.lineWidth = '2';
  ctx.strokeStyle = 'black';
  ctx.rect(tile.x, tile.y, tile.width, tile.height);
  ctx.stroke();

  // Draw the number.
  ctx.fillStyle = tile.color;
  ctx.font = "20px Arial";
  let textX = tile.x + tile.width / 3
  // Make sure 2-digit numbers are nice and centered(-ish).
  if (tile.number > 9) {
    textX = tile.x + tile.width / 4
  }
  ctx.fillText(tile.number, textX, tile.y + tile.height / 2);
}

function drawButton(button) {
  // Draw the color button rectangle.
  if (button.color) {
    ctx.fillStyle = button.color;
    ctx.fillRect(button.x, button.y, button.width, button.height);
  } else {
    // Draw the textButton rectangle
    ctx.beginPath();
    ctx.lineWidth = '2';
    ctx.strokeStyle = 'black';
    ctx.rect(button.x, button.y, button.width, button.height);
    ctx.stroke();

    // Draw the textButton text
    ctx.fillStyle = 'black';
    ctx.font = "10px Arial";
    ctx.fillText(button.text, button.x+button.width/6, button.y+button.height/1.5);

  }
}

function onMouseDown(event) {
  const rect = canvas.getBoundingClientRect();
  const mouseX = event.clientX - rect.left;
  const mouseY = event.clientY - rect.top;
  game.checkMouseDown(mouseX, mouseY);
}

function onMouseUp(event) {
  const rect = canvas.getBoundingClientRect();
  const mouseX = event.clientX - rect.left;
  const mouseY = event.clientY - rect.top;
  game.checkMouseUp(mouseX, mouseY);
}

function onMouseMove(event) {
  const rect = canvas.getBoundingClientRect();
  const mouseX = event.clientX - rect.left;
  const mouseY = event.clientY - rect.top;
  game.checkMouseMove(mouseX, mouseY);
}

/*
 * Functions to handle the touch events.
 * We simply convert them to an equivalent mouse event, and let the event dispatcher handle the rest.
 *
 */
function onTouchStart(event) {
  const mouseEvent = new MouseEvent("mousedown", {
    clientX: event.touches[0].clientX,
    clientY: event.touches[0].clientY
  });
  canvas.dispatchEvent(mouseEvent);
}

function onTouchEnd(event) {
  const mouseEvent = new MouseEvent("mouseup", {
    clientX: event.touches[0].clientX,
    clientY: event.touches[0].clientY
  });
  canvas.dispatchEvent(mouseEvent);
}

function onTouchMove(event) {
  const mouseEvent = new MouseEvent("mousemove", {
    clientX: event.touches[0].clientX,
    clientY: event.touches[0].clientY
  });
  canvas.dispatchEvent(mouseEvent);
}

/*
 * Function to prevent the webpage from scrolling, to be used when touching the canvas on mobile.
 */
function preventScrolling(event) {
  alert('a');
  if (event.target == canvas) {
    alert('b');
    e.preventDefault();
  }
}

function draw() {
  // Clear all (TODO: optimize this!)
  ctx.clearRect(0, 0, 1000, 650);

  // Draw the field lines.
  drawField();

  // Draw all top tiles.
  game.topTiles.forEach(function(tile, index, array) {
    drawTile(tile);
  });

  // Draw all board tiles.
  game.boardTiles.forEach(function(tile, index, array) {
    drawTile(tile);
  });

  // Draw all player tiles.
  game.playerTiles.forEach(function(tile, index, array) {
    drawTile(tile);
  });

  // Draw all buttons.
  game.buttons.forEach(function(button, index, array) {
    drawButton(button);
  });

  // Draw tile, if it is being dragged.
  if (game.dragTile) {
    drawTile(game.dragTile);
  }

  // Keep drawing.
  window.requestAnimationFrame(draw);
}

function main(tframe) {
  // Fetch canvas information.
  canvas = document.getElementById('canvas');
  ctx = canvas.getContext('2d');

  // Set the event listeners for interacting with the canvas.
  canvas.onmousedown = onMouseDown;
  canvas.onmouseup = onMouseUp;
  canvas.onmousemove = onMouseMove;

  canvas.ontouchstart = onTouchStart;
  canvas.ontouchend = onTouchEnd;
  canvas.ontouchmove = onTouchMove;

  // We also want to prevent scrolling when touching the canvas on mobile.
  // Thanks to: http://bencentra.com/code/2014/12/05/html5-canvas-touch-events.html
  document.body.addEventListener("touchstart", function (e) {
    if (e.target == canvas) {
      e.preventDefault();
    }
  }, false);
  document.body.addEventListener("touchend", function (e) {
    if (e.target == canvas) {
      e.preventDefault();
    }
  }, false);
  document.body.addEventListener("touchmove", function (e) {
    if (e.target == canvas) {
      e.preventDefault();
    }
  }, false);

  // Create a new game.
  game = new Game();

  // Start drawing.
  window.requestAnimationFrame(draw);
}


main();
