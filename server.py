from http.server import HTTPServer, BaseHTTPRequestHandler

from io import BytesIO
import json
import os
from pyidp3.typedIDP import IDP


def runIDP(data):
    tilenames = []
    start_in_hand = []
    tile_color = {}
    tile_number = {}
    joker = []
    print('a', data)

    # Create the information needed for the struct.
    # These are arrays and dicts that represent type values or function
    # assignments etc
    for tile in data['board']:
        if tile['tile']['number'] != 'jkr':
            tilenames.append(tile['name'])
            tile_color[tile['name']] = tile['tile']['color']
            tile_number[tile['name']] = tile['tile']['number']
        else:
            joker.append(tile['name'])
            # If the tile is a joker tile, just set it to Red 1.
            tilenames.append(tile['name'])
            tile_color[tile['name']] = 'red'
            tile_number[tile['name']] = 1

    for tile in data['player']:
        if tile['tile']['number'] != 'jkr':
            tilenames.append(tile['name'])
            start_in_hand.append(tile['name'])
            tile_color[tile['name']] = tile['tile']['color']
            tile_number[tile['name']] = tile['tile']['number']
        else:
            joker.append(tile['name'])
            # If the tile is a joker tile, just set it to Red 1.
            tilenames.append(tile['name'])
            tile_color[tile['name']] = 'red'
            tile_number[tile['name']] = 1

    print('color', tile_color)
    print('number', tile_number)
    # return

    # Init IDP and generate the script.
    idp = IDP('./idp/usr/local/bin/idp')
    idp.nbmodels = 1

    idp.Type("Tile", tilenames)  # TODO
    idp.Type("Color", ['blue', 'red', 'orange', 'black'])
    idp.Type("Number", list(range(0, 14)))
    idp.Type("Group", list(range(1, 10)))
    idp.Type("Run", list(range(1, 10)))

    idp.Function("color_of(Tile): Color")
    idp.Function("number_of(Tile): Number")
    idp.Function("tile_color(Tile): Color", tile_color)
    idp.Function("tile_number(Tile): Number", tile_number)

    idp.Predicate("joker(Tile)", joker)
    idp.Predicate("in_run(Run, Tile)")
    idp.Predicate("in_group(Group, Tile)")
    idp.Function("nb_in_run(Run): Number")
    idp.Function("nb_in_group(Group): Number")
    idp.Function("highest_in_run(Run): Number")

    idp.Predicate("in_hand(Tile)")
    idp.Predicate("start_in_hand(Tile)", start_in_hand)
    idp.Predicate("placed(Tile)")
    idp.Constant("nb_in_hand: Number")
    idp.Constant("nb_in_start: Number")

    # Count nb of tiles in groups and runs.
    idp.Constraint("!g[Group]: nb_in_group(g) = #{t[Tile]: in_group(g, t)}.",
                   True)
    idp.Constraint("!r[Run]: nb_in_run(r) = #{t[Tile]: in_run(r, t)}.", True)

    # Sets are either empty or have min three tiles.
    idp.Constraint("!g[Group]: nb_in_group(g) = 0 | nb_in_group(g) >= 3.",
                   True)
    idp.Constraint("!r[Run]: nb_in_run(r) = 0 | nb_in_run(r) >= 3.", True)

    # A group has the tiles of the same numbers, different colors.
    idp.Constraint("!g[Group] t1[Tile] t2[Tile]: t1 ~= t2 & in_group(g, t1) &"
                   " in_group(g, t2) => number_of(t1) = number_of(t2) &"
                   " color_of(t1) ~= color_of(t2).", True)

    # A run has tiles with consecutive numbers and same colors.
    idp.Constraint("!r[Run] t1[Tile] t2[Tile]: t1 ~= t2 & in_run(r, t1) &"
                   " in_run(r, t2) => color_of(t1) = color_of(t2).", True)
    idp.Constraint("!r[Run] t1[Tile]: in_run(r, t1)  => ((?t2: in_run(r, t2) &"
                   " number_of(t2) = number_of(t1) + 1)"
                   " | (?=0t2: in_run(r, t2) & t2 ~= t1 & number_of(t1) < number_of(t2)))"
                   " & ?=0t2: in_run(r, t2) & t2 ~= t1 & number_of(t2) = number_of(t1).",
                   True)

    # Every tile is in exactly one group or one run.
    idp.Constraint("!t[Tile]: (#{g[Group]: in_group(g, t)} = 1 & #{r[Run]:"
                   " in_run(r, t)} = 0 & ~in_hand(t)) |"
                   " (#{g[Group]: in_group(g, t)} = 0 & #{r[Run]:"
                   " in_run(r, t)} = 1 & ~in_hand(t)) | (#{g[Group]:"
                   " in_group(g, t)} = 0 &"
                   " #{r[Run]: in_run(r, t)} = 0 & in_hand(t)).", True)

    # Only tiles in hand at the start are allowed in hand at the end --
    # no taking tiles off the board.
    idp.Constraint("!t[Tile]: in_hand(t) => start_in_hand(t).", True)

    idp.Constraint("!t[Tile]: placed(t) <=> start_in_hand(t) & ~in_hand(t).",
                   True)

    idp.Constraint("nb_in_hand = #{t[Tile]: in_hand(t)}.", True)
    idp.Constraint("nb_in_start = #{t[Tile]: start_in_hand(t)}.", True)

    # All non-joker tiles have a set color and number.
    idp.Constraint("!t[Tile]: tile_color(t) = color_of(t) <=> ~joker(t).",
                   True)
    idp.Constraint("!t[Tile]: tile_number(t) = number_of(t) <=> ~joker(t).",
                   True)

    sols = idp.minimize("nb_in_hand")

    return sols


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/':
            path = 'rummikub.html'
        else:
            # Strip the '/' in the front.
            path = self.path[1:]
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        with open(path, 'r') as fp:
            html = fp.read()
        # self.wfile.write(b'Hello, world!')
        self.wfile.write(bytes(html, 'utf-8'))

    def do_POST(self):
        # Receive POST request.
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)

        # Decode the json into a dict
        data = json.loads(body)

        # Convert to IDP, and run to get solution.
        print('STARTING IDP')
        sols = runIDP(data)
        print(sols[0]['in_group'])
        print(sols[0]['in_run'])
        print(sols[0]['in_hand'])
        print(f"Nb left in hand {sols[0]['nb_in_hand']}")
        print('DONE')

        # Create response json
        groups = {}
        try:
            for group_id, tile in sols[0]['in_group']:
                if group_id in groups:
                    groups[group_id].append(tile)
                else:
                    groups[group_id] = [tile]
        except ValueError:
            print("No groups")

        runs = {}
        try:
            for run_id, tile in sols[0]['in_run']:
                if run_id in runs:
                    runs[run_id].append(tile)
                else:
                    runs[run_id] = [tile]
        except ValueError:
            print("No sets")

        response_json = {'group': groups, 'run': runs, 'in_hand': sols[0]['in_hand']}

        # Send response.
        self.send_response(200)
        self.end_headers()
        response = BytesIO()
        response.write(bytes(json.dumps(response_json), 'utf-8'))
        self.wfile.write(response.getvalue())


if __name__ == "__main__":
    if 'PORT' in os.environ:
        port = int(os.environ['PORT'])
    else:
        port = 8000
    httpd = HTTPServer(('0.0.0.0', port), SimpleHTTPRequestHandler)
    print('Started http server at http://localhost:8000')
    httpd.serve_forever()
