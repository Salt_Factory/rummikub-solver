# Rummikub Solver

This repo contains a logic-based rummikub solver, with a browser-based interface.

The solver itself uses the [IDP reasoning system](https://dtai.cs.kuleuven.be/software/idp) to find the most optimal ways to play Rummikub.
Given a hand of tiles (that you have on your board), and the tiles that are already on the table, it tries to get rid of as many of your hand tiles as possible.

The interface is 100% authentic JavaScript, and is driven by a Python back-end that manages the interaction with the IDP system.

# Demo

![Demo of solver](./demo/demo.gif)

# Try it out

You can try out an online version of the solver by going by clicking [here](https://rummikub-solver.herokuapp.com/).


# Installation

Currently, the IDP system only supports Linux & MacOS.
Using the interface is as simple as installing the `pyidp3` Python package (e.g. via Pip), and then running the following command:

```
python3 server.py
```


